<?php
include("../db.php");

if($_SESSION["u"] == "") header("location:login.php");

if($_GET["p"] == "logout") {
    session_destroy();
    header("location:login.php");
}

if($_GET["p"] == "") $p = "home";
else $p = $_GET["p"];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Aplikasi Persediaan</title>
    <link rel="stylesheet" href="../css/style.css">
    <script src="../js/jquery.min.js"></script>
</head>
<body>
    <div class="container">
        <div class="header">
            <img src="../gambar/header.png" alt="">
        </div>
        <div class="menu">
            <ul>
                <li><a href="index.php">Home</a></li>
                <li><a href="index.php?p=daftar-barang">Daftar Barang</a></li>
                <li>
                    <a href="#">Transaksi</a>
                    <ul>
                        <li><a href="index.php?p=transaksi-masuk">Transaksi Masuk</a></li>
                        <li><a href="index.php?p=transaksi-keluar">Transaksi Keluar</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">Laporan Transaksi</a>
                    <ul>
                        <li><a href="index.php?p=laporan-per-barang">Laporan Per Barang</a></li>
                        <li><a href="index.php?p=laporan-per-pengguna">Laporan Per Pengguna</a></li>
                        <li><a href="index.php?p=laporan-bulanan">Laporan Bulanan</a></li>
                    </ul>
                </li>
                <li><a href="index.php?p=logout">Logout</a></li>
                <div class="clear"></div>
            </ul>
        </div>
        <?php
        include($p.".php");
        ?>
        <div class="footer">
            <p>Copyright &copy; 2016 Kantor Imigrasi Kelas II Cirebon.</p>
        </div>
    </div>
</body>
</html>
