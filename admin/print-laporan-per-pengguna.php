<?php
include("../db.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Print</title>
    <style>
        .tabel {
            width: 100%;
            border-collapse: collapse;
            border: 1px solid #000;
        }
        .tabel td {
            border: 1px solid #000;
            padding: 10px;
        }
    </style>
    <script>
        window.print();
    </script>
</head>
<body>   
    <h3>Laporan Persediaan Barang Kantor Imigrasi Kelas II Cirebon</h3>

    <?php if($_GET["pengguna"] != "") { ?>
        <?php
        $query = mysql_query("select * from pengguna where id=$_GET[pengguna]");
        $result = mysql_fetch_array($query);
        ?>
        <table class="tabel">
            <tr>
                <td width="22%">Nama Pengguna</td>
                <td><?php echo $result["nama"] ?></td>
            </tr>
        </table>
        <br>
        <table class="tabel">
            <tr>
                <td width="7%">No</td>
                <td width="15%">Tanggal</td>
                <td>Nama Barang</td>
                <td width="5%">Masuk</td>
                <td width="5%">Keluar</td>
            </tr>
            <?php
            $no = 1;
            $s = 0;
            $query = mysql_query("select
            transaksi.tipe_trans,
            transaksi.tanggal,
            barang.nama as namabrg,
            detil_trans.jumlah,
            barang.satuan
            from detil_trans
            join barang on barang.id=detil_trans.barang
            join transaksi on transaksi.id=detil_trans.transaksi
            join pengguna on pengguna.id=transaksi.pengguna
            where transaksi.pengguna=$_GET[pengguna]
            order by
            transaksi.tanggal ASC,
            transaksi.tipe_trans DESC
            ") or die(mysql_error());
            while($result = mysql_fetch_array($query)) {
                echo "<tr>";
                echo "<td>$no</td>";
                echo "<td>$result[tanggal]</td>";
                echo "<td>$result[namabrg]</td>";

                if($result["tipe_trans"] == "masuk") {
                    $s += $result["jumlah"];
                    echo "<td>$result[jumlah]</td>";
                    echo "<td>-</td>";
                } else {
                    $s -= $result["jumlah"];
                    echo "<td>-</td>";
                    echo "<td>$result[jumlah]</td>";
                }
                echo "<tr>";
                $no++;
            }
            ?>
        </table>
        <br><br><br>
        <table width="100%">
            <tr>
                <td width="75%"></td>
                <td>
                    Mengetahui,<br>
                    Karus Umum
                    <br><br><br><br><br>
                    Jubaedah, S.Sos
                </td>
            </tr>
        </table>
    <?php } ?>
</body>
</html>