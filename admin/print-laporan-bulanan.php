<?php
include("../db.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Print</title>
    <style>
        .tabel {
            width: 100%;
            border-collapse: collapse;
            border: 1px solid #000;
        }
        .tabel td {
            border: 1px solid #000;
            padding: 10px;
        }
    </style>
    <script>
        window.print();
    </script>
</head>
<body>   
    <h3>Laporan Persediaan Barang Kantor Imigrasi Kelas II Cirebon</h3>

    <?php
    if($_GET["bulan"] != "" && $_GET["tahun"] != "") {
        $result = mysql_fetch_array($query);
        ?>
        <table class="tabel">
            <tr>
                <td width="22%">Bulan</td>
                <td>
                    <?php
                    $bulan = array(
                        1 => "Januari",
                        "Februari",
                        "Maret",
                        "April",
                        "Mei",
                        "Juni",
                        "Juli",
                        "Agustus",
                        "September",
                        "Oktober",
                        "November",
                        "Desember",
                    );
                    echo $bulan[$_GET["bulan"]];
                    ?>
                </td>
            </tr>
            <tr>
                <td>Tahun</td>
                <td><?php echo $_GET["tahun"] ?></td>
            </tr>
        </table>
        <br>
        <table class="tabel">
            <tr>
                <td width="7%">No</td>
                <td width="15%">Tanggal</td>
                <td>Nama Barang</td>
                <td width="15%">Pengguna</td>
                <td width="5%">Masuk</td>
                <td width="5%">Keluar</td>
            </tr>
            <?php
            $no = 1;
            $s = 0;
            $tgl = $_GET["tahun"]."-".sprintf("%02d", $_GET["bulan"]);
            $query = mysql_query("select 
            transaksi.tipe_trans,
            transaksi.tanggal,
            barang.nama namabrg,
            pengguna.nama namap,
            detil_trans.jumlah
            from transaksi
            join detil_trans on detil_trans.transaksi=transaksi.id
            join barang on barang.id=detil_trans.barang
            join pengguna on pengguna.id=transaksi.pengguna
            where tanggal like '$tgl-%'
            order by
            transaksi.tanggal ASC,
            transaksi.tipe_trans DESC
            ") or die(mysql_error());
            while($result = mysql_fetch_array($query)) {
                echo "<tr>";
                echo "<td>$no</td>";
                echo "<td>$result[tanggal]</td>";
                echo "<td>$result[namabrg]</td>";
                echo "<td>$result[namap]</td>";

                if($result["tipe_trans"] == "masuk") {
                    echo "<td>$result[jumlah]</td>";
                    echo "<td>-</td>";
                } else {
                    echo "<td>-</td>";
                    echo "<td>$result[jumlah]</td>";
                }
                echo "<tr>";
                $no++;
            }
            ?>
        </table>
        <br><br><br>
        <table width="100%">
            <tr>
                <td width="75%"></td>
                <td>
                    Mengetahui,<br>
                    Karus Umum
                    <br><br><br><br><br>
                    Jubaedah, S.Sos
                </td>
            </tr>
        </table>
    <?php } ?>
</body>
</html>