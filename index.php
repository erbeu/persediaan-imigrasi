<?php
include("db.php");

if($_SESSION["pengguna"] == "") header("location:login.php");

if($_GET["p"] == "logout") {
    session_destroy();
    header("location:login.php");
}

if($_GET["p"] == "") $p = "transaksi-keluar";
else $p = $_GET["p"];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Aplikasi Persediaan</title>
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery.min.js"></script>
</head>
<body>
    <div class="container">
        <div class="header">
            <img src="gambar/header.png" alt="">
        </div>
        <?php
        include($p.".php");
        ?>
        <div class="footer">
            <p>Copyright &copy; 2016 Kantor Imigrasi Kelas II Cirebon.</p>
        </div>
    </div>
</body>
</html>